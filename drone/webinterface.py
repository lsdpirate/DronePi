from flask import Flask, request, render_template
import drone_movement as DM
import sys
import web_command_bindings as wcb 

app = Flask('RoverPi')

@app.route('/index')
def index():
	return render_template('index.html', command_bindings = wcb.command_bindings)

@app.route('/command_reader', methods = ['POST'])
def command_reader():
	command_type = request.form['command_type']
	command = str(request.form['command'])
	if command_type == 'AXS':
		print(type(command))
		DM.set_motor_speed(int(command[:3]), int(command[3:]))
	return "Command received"

@app.route('/control')
def control_page():
	return render_template('joystick_control.html')

def run_server(debug = False):
	app.debug = debug
	app.run()

if __name__ == "__main__":
	debug = '--debug' in sys.argv
	run_server(debug)
